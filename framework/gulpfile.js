var gulp         =  require('gulp');
var cssmin       =  require('gulp-cssmin');
var sass         =  require('gulp-sass');
var rename       =  require('gulp-rename');
var uglify       =  require('gulp-uglify');
var watch        =  require('gulp-watch');
var pump         =  require('pump');
var autoprefixer =  require('gulp-autoprefixer');
var rev          =  require('gulp-rev');


var SCSS_FILENAME = 'sdx';
var JS_FILENAME   = 'sdx';


gulp.task('compress', function (cb) {
    pump([
        gulp.src('src/js/'+JS_FILENAME+'.js'),
        uglify(),
        rename({suffix:'.min'}),
        //gulp.dest('js/'),
        gulp.dest('dist/js/')
        .on('end', function () {console.log('JS Minified')})
    ],cb);
}); 

gulp.task('sass', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', function (err) { console.log(err); }))
        //.pipe(gulp.dest('style/css'))
        .pipe(gulp.dest('dist/css'))
        .on('end', function () {
            // after end 'sass' task, pipe 'autoprefix' task
            gulp.src('dist/css/'+SCSS_FILENAME+'.css')
                .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
                .pipe(gulp.dest('dist/css'))
                .on('end', function () {
                // after end 'autoprefix' task, pipe 'minify' task
                    gulp.src('dist/css/'+SCSS_FILENAME+'.css')
                        .pipe(cssmin())
                        .pipe(rename({ suffix: '.min' }))
                        //.pipe(gulp.dest('style/css'))
                        .pipe(gulp.dest('dist/css')) 
                        .on('end', function(){ console.log('CSS Minified'); })
        })
    })
});

// $> gulp rev 
// gulp.task('rev', function () {
//     gulp.src('src/style/css/'+SCSS_FILENAME+'.min.css')
//     .pipe(rev())
//     .pipe(gulp.dest('style/css'))
//     .on('end', function () { console.log('CSS Rev')})
// });

gulp.task('watch', function () {
    gulp.watch('src/scss/*.scss', ['sass']);
    gulp.watch('src/js/*.js', ['compress']);
});


gulp.task('default', ['compress','sass','watch'])